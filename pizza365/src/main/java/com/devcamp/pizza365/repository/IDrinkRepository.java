package com.devcamp.pizza365.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Integer> {
  Optional<CDrink> findById(int id);

  Optional<CDrink> findByMaNuocUong(String maNuocUong);
}