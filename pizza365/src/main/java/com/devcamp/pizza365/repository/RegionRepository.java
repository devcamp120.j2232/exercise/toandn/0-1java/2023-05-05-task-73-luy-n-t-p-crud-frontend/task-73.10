package com.devcamp.pizza365.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizza365.model.CRegion;

@Repository
public interface RegionRepository extends JpaRepository<CRegion, Long> {
	Optional<CRegion> findById(long id);

	Optional<CRegion> findByRegionCode(String regionCode);

	CRegion findByRegionName(String regionName);

	List<CRegion> findByCountryId(Long countryId);

	List<CRegion> findByCountryCountryCode(String countryCode);

	List<CRegion> findByCountryCountryName(String countryName);

	Optional<CRegion> findByIdAndCountryId(Long id, Long instructorId);
}
