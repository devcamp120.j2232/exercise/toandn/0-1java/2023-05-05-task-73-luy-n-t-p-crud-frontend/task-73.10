package com.devcamp.pizza365.model;

import javax.persistence.*;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

import javax.persistence.Column;
import javax.validation.constraints.*;

@Entity
@Table(name = "pizza_order")
public class COrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Fill mã order")
    @Size(min = 1, message = "Mã order có ít nhất 1 ký tự")
    @Column(name = "order_code", unique = true)
    private String orderCode;

    @NotNull(message = "Fill sizze pizza")
    @Size(min = 1, message = "Size pizza có ít nhất 1 ký tự")
    @Column(name = "pizza_size")
    private String pizzaSize;

    @NotNull(message = "Fill type pizza")
    @Size(min = 1, message = "type pizza có ít nhất 1 ký tự")
    @Column(name = "pizza_type")
    private String pizzaType;

    @NotNull(message = "fill code voucher")
    @Size(min = 1, message = "Voucher code có ít nhất 1 ký tự")
    @Column(name = "voucher_code")
    private String voucherCode;

    @NotNull(message = "fill price")
    @Range(min = 10000, message = "Price phải lớn hơn 10000 đồng")
    @Column(name = "price")
    private long price;

    @NotNull(message = "fill paid")
    @Range(min = 10000, message = "Paid phải lớn hơn 10000 đồng")
    @Column(name = "paid")
    private long paid;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created_at", nullable = true, updatable = false)
    @CreatedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "updated_at", nullable = true)
    @LastModifiedDate
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date updated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    private CUser user;

    public COrder() {
        // super();
    }

    public COrder(long id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid) {
        this.id = id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPaid() {
        return paid;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public CUser getUser() {
        return user;
    }

    public void setUser(CUser user) {
        this.user = user;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
