package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CCountry;
import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CRegion;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.CountryRepository;
import com.devcamp.pizza365.repository.RegionRepository;
import com.devcamp.pizza365.service.RegionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/region")
public class RegionController {
	@Autowired
	RegionRepository regionRepository;
	@Autowired
	RegionService regionService;

	@Autowired
	CountryRepository countryRepository;

	// lấy danh sách region
	@GetMapping("/all")
	public ResponseEntity<List<CRegion>> getAllRegions() {
		try {
			return new ResponseEntity(regionService.getAllRegion(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// get region detail by id
	@GetMapping("/detail/{id}")
	public ResponseEntity<Object> getRegionById(@PathVariable(name = "id", required = true) long id) {
		Optional<CRegion> region = regionRepository.findById(id);
		if (region.isPresent()) {
			return new ResponseEntity<>(region, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	// create new region with country id
	@PostMapping("/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") long id, @Valid @RequestBody CRegion pRegion) {
		try {
			Optional<CCountry> country = countryRepository.findById(id);
			Optional<CRegion> _checkRegionCode = regionRepository.findByRegionCode(pRegion.getRegionCode());
			CRegion newRegion = new CRegion();
			if (_checkRegionCode.isPresent()) {
				return ResponseEntity.unprocessableEntity().body("code region already exsit!");
			} else {
				newRegion.setRegionCode(pRegion.getRegionCode());
				newRegion.setRegionName(pRegion.getRegionName());
				newRegion.setCountry(country.get());
				CRegion _region = regionRepository.save(newRegion);
				try {
					return new ResponseEntity<>(_region, HttpStatus.CREATED);
				} catch (Exception e) {
					return ResponseEntity.unprocessableEntity()
							.body("Failed to create specified region: " + e.getCause().getMessage());
				}
			}

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	// update region by country id
	@PutMapping("/update/{countryId}/{regionId}")
	public ResponseEntity<Object> updateRegion(@PathVariable(name = "countryId") long countryId,
			@PathVariable(name = "regionId") long regionId,
			@Valid @RequestBody CRegion updateRegion) {
		try {
			Optional<CCountry> country = countryRepository.findById(countryId);
			Optional<CRegion> region = regionRepository.findById(regionId);
			CRegion _region = region.get();
			_region.setRegionCode(updateRegion.getRegionCode());
			_region.setRegionName(updateRegion.getRegionName());
			_region.setCountry(country.get());
			try {
				return ResponseEntity.ok(regionRepository.save(_region));
			} catch (Exception e) {
				return ResponseEntity.unprocessableEntity()
						.body("Failed to Update specified region: " + e.getCause().getMessage());
			}

		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<Object>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}

	@CrossOrigin
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
	public List<CRegion> getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId);
	}

	@CrossOrigin
	@GetMapping("/country/regions")
	public List<CRegion> countRegionByCountryCode(@RequestParam(value = "countryCode") String countryCode) {
		return regionRepository.findByCountryCountryCode(countryCode);
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
	public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,
			@PathVariable(value = "id") Long regionId) {
		return regionRepository.findByIdAndCountryId(regionId, countryId);
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/count")
	public int countRegionByCountryId(@PathVariable(value = "countryId") Long countryId) {
		return regionRepository.findByCountryId(countryId).size();
	}

	@CrossOrigin
	@GetMapping("/check/{id}")
	public boolean checkRegionById(@PathVariable Long id) {
		return regionRepository.existsById(id);
	}
}
